###Changelog


#### 20210831.1
* Updated versions

#### 20210830.1
* Updated files for Divi Builder 4.10.x
* Changed tested up to 5.8

#### 20200209.1
* Created a basic child theme
* added header.php, footer.php and functions.php
* added css for the AP login widget 
* added css for the The Events Calendar
* added css for having the footer bottom bar fixed 
* header.php - added target blank to the social icons.
* footer.php - added target blank to the social icons.
* footer.php - changes the copyright to site title/name.
* images - override the default image size, don't like the cropping of images in Divi
