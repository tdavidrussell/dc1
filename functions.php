<?php

/**
 * Child Theme Functions
 *
 * Functions or examples that may be used in a child them. Don't for get to edit them, to get them working.
 *
 * @category            WordPress_Theme
 * @package             DC2_Divi_Child
 * @subpackage          theme
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Load parent stylesheet....
 */
function rone_enqueue_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'rone_enqueue_theme_styles' );


/**
 * The gallery module not recognise the image orientation.
 * All images reduced to the fixed sizes and may be cropped.
 * We can change those fixed sizes. Please add the following
 * code to the functions.php :
 *
 * @link ET Forums: https://www.elegantthemes.com/forum/viewtopic.php?f=187&t=470086&p=2610589&hilit=image+sizes+gallery+image+cropped#p2610589
 *
 * @param $height
 *
 * @return string
 */
function ro_gallery_size_h( $height ) {
	return '1080';
}

add_filter( 'et_pb_gallery_image_height', 'ro_gallery_size_h' );

function ro_gallery_size_w( $width ) {
	return '9999';
}

add_filter( 'et_pb_gallery_image_width', 'ro_gallery_size_w' );

/**
 * Stop cropping the height of images
 * @link https://divibooster.com/stop-divi-from-cropping-feature-post-heights/
 *
 */
//add_filter( 'et_theme_image_sizes', 'ro_remove_featured_post_cropping' );
function ro_remove_featured_post_cropping( $sizes ) {
	if ( isset( $sizes['1080x675'] ) ) {
		unset( $sizes['1080x675'] );
		$sizes['1080x9998'] = 'et-pb-post-main-image-fullwidth';
	}

	return $sizes;
}

function rone_divi_sidebar_menu( $atts ) {
	global $wp_widget_factory;

	extract( shortcode_atts( array(
		'widget_name' => false
	), $atts ) );

	$mname = $atts['mname'];

	ob_start();

	if ( '' !== $mname ) { ?>
        <div id="secondary" class="secondary">

            <nav id="site-navigation" class="main-navigation" role="navigation">
				<?php
				// Primary navigation menu.
				wp_nav_menu( array(
					'menu_class' => 'nav-menu',
					//'menu' => 'main_menu',
					'menu'       => $mname,
					//'theme_location' => 'primary-menu',

				) );
				?>
            </nav><!-- .main-navigation -->

			<?php /** if ( has_nav_menu( 'social' ) ) : ?>
			 * <nav id="social-navigation" class="social-navigation" role="navigation">
			 * <?php
			 * // Social links navigation menu.
			 * wp_nav_menu( array(
			 * 'theme_location' => 'social',
			 * 'depth'          => 1,
			 * 'link_before'    => '<span class="screen-reader-text">',
			 * 'link_after'     => '</span>',
			 * ) );
			 * ?>
			 * </nav><!-- .social-navigation -->
			 * <?php endif;  */ ?>


        </div><!-- .secondary -->
		<?php
		$output = ob_get_contents();
		ob_end_clean();

		echo $output;;
	}

}

add_shortcode( 'rone_divi_sb_menu', 'rone_divi_sidebar_menu' );
?>